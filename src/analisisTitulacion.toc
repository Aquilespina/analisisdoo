\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducción}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Objetivo del documento}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Estructura del documento}{1}{section.1.2}%
\contentsline {chapter}{\numberline {2}Modelo de negocio}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Reglas de negocio}{3}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Reglas derivadas del sistema}{3}{subsection.2.1.1}%
\contentsline {subsubsection}{RN-1 Definición de producto}{3}{section*.2}%
\contentsline {subsubsection}{RN-2 Formato de codigo de barras.}{4}{section*.3}%
\contentsline {subsubsection}{RN-3 Venta con receta médica.}{4}{section*.4}%
\contentsline {subsubsection}{RN-4 Venta con receta médica.}{4}{section*.5}%
\contentsline {subsubsection}{RN-5 Correo vàlido.}{4}{section*.6}%
\contentsline {subsubsection}{RN-6 Correo vàlido.}{5}{section*.7}%
\contentsline {chapter}{\numberline {3}Pantallas}{7}{chapter.3}%
\contentsline {chapter}{\numberline {4}Modelo de comportamiento}{9}{chapter.4}%
